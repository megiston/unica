module Unica
    class Automobili
        attr_accessor :master
        PATH = "/automobili"

        def initialize(master)
          @master = master
        end

        def carrozeria_options
          ["City car", "Berlina", "Station wagon", "Monovolume", "SUV/Fuoristrada", "Coupé", "Cabrio", "Furgoni/Van", "Altro"]
        end

        def alimentazione_options
          ["Benzina","Diesel","Gas/GPL","Metano","Elettrica/Benzina","Elettrica/Diesel","Etanolo","Elettrica","Idrogeno","Altro"]
        end

        def trazione_options
          ["4x2", "4x4"]
        end

        def cambio_options
          ["manuale", "automatico", "semiautomatico"]
        end

        def colore_esterno_options
          ["n/d", "Bianco", "Argento", "Grigio", "Antracite", "Nero", "Blu", "Rosso", "Arancione", "Avorio", "Azzurro", "Beige", "Bordeaux", "Bronzo", "Giallo", "Marrone", "Ocra", "Oro", "Panna", "Perla", "Quarzo", "Rosa", "Sabbia", "Verde", "Viola"]
        end

        def condizione_options
          ["Nuovo", "Usato", "KM0", "Incidentato"]
        end

        def porte_options
          ["2/3", "4/5", "6/7"]
        end

        def consegna_options
          ["pronta consegna", "in arrivo", "su ordinazione"]
        end

        def crea_modello(id_marca:nil, nome_modello:, codice_marca:nil)
            _keys = method(__method__).parameters.collect(&:last)
            # _optional_keys = method(__method__).parameters.to_h[:key].collect
            _params = {}
            _keys.each do |par|
              _params[par] = eval(par.to_s) unless eval(par.to_s).nil?
            end
            return @master.call(File.join("", "/modelli/crea_modello"), _params)
        end

        def crea_automobile(id_marca: nil ,codice_marca: nil, nome_marca: nil, id_modello: nil ,codice_modello:nil, nome_modello: nil ,targa: , id_esterno:, versione: nil, carrozzeria: nil, porte: nil, condizione: "Usato", km_percorsi:, anno: nil, mese: nil, nazione: nil, provincia: nil, consegna: nil, alimentazione: nil, cilindrata: , cambio: , trazione: nil, potenza_cv: nil, potenza_kw: nil, emissioni_co2: ,colore_esterno: nil, colore_interno: nil, prezzo_listino: nil, iva_inclusa: , iva_esposta: ,descrizione_danni: nil, valore_danni: nil, dotazione_serie: nil, optionals: nil, descrizione_iva: nil,  note: nil, data_inizio_annuncio: "", data_fine_annuncio: "", prezzo_commerciante: , prezzo_privato: , images: [], annuncio_approvato: 0 )
            _keys = method(__method__).parameters.collect(&:last)
            # _optional_keys = method(__method__).parameters.to_h[:key].collect
            _params = {}
            _keys.each do |par|
              _params[par] = eval(par.to_s) unless eval(par.to_s).nil?
            end
            return @master.call(File.join(PATH, "crea_automobile"), _params)
        end

        def crea_automobile(id_marca: nil ,codice_marca: nil, nome_marca: nil, id_modello: nil ,codice_modello:nil, nome_modello: nil ,targa: , id_esterno:, versione: nil, carrozzeria: nil, porte: nil, condizione: "Usato", km_percorsi:, anno: nil, mese: nil, nazione: nil, provincia: nil, consegna: nil, alimentazione: nil, cilindrata: , cambio: , trazione: nil, potenza_cv: nil, potenza_kw: nil, emissioni_co2: ,colore_esterno: nil, colore_interno: nil, prezzo_listino: nil, iva_inclusa: , iva_esposta: ,descrizione_danni: nil, valore_danni: nil, dotazione_serie: nil, optionals: nil, descrizione_iva: nil, note: nil, data_inizio_annuncio: "", data_fine_annuncio: "", prezzo_commerciante: , prezzo_privato: , images: [], annuncio_approvato: 0 )
            _keys = method(__method__).parameters.collect(&:last)
            # _optional_keys = method(__method__).parameters.to_h[:key].collect
            _params = {}
            _keys.each do |par|
              _params[par] = eval(par.to_s) unless eval(par.to_s).nil?
            end
            return @master.call(File.join(PATH, "crea_automobile"), _params)
        end

        def modifica(id_automobile_interno: , id_automobile_esterno:nil, id_marca: nil ,codice_marca: nil, nome_marca: nil, id_modello: nil ,codice_modello:nil, nome_modello: nil ,targa: , id_esterno:, versione: nil, carrozzeria: nil, porte: nil, condizione: "Usato", km_percorsi:, anno: nil, mese: nil, nazione: nil, provincia: nil, consegna: nil, alimentazione: nil, cilindrata: , cambio: , trazione: nil, potenza_cv: nil, potenza_kw: nil, emissioni_co2: ,colore_esterno: nil, colore_interno: nil, prezzo_listino: nil, iva_inclusa: , iva_esposta: ,descrizione_danni: nil, valore_danni: nil, dotazione_serie: nil, optionals: nil, descrizione_iva: nil, note: nil, data_inizio_annuncio: "", data_fine_annuncio: "", prezzo_commerciante: , prezzo_privato: , images: [], annuncio_approvato: 0 )
            _keys = method(__method__).parameters.collect(&:last)
            # _optional_keys = method(__method__).parameters.to_h[:key].collect
            _params = {}
            _keys.each do |par|
              _params[par] = eval(par.to_s) unless eval(par.to_s).nil?
            end
            return @master.call(File.join(PATH, "modifica_automobile"), _params)
        end


        def cancella_automobile(id_automobile_esterno: , id_automobile_interno: nil)
            _keys = method(__method__).parameters.collect(&:last)
            # _optional_keys = method(__method__).parameters.to_h[:key].collect
            _params = {}
            _keys.each do |par|
              _params[par] = eval(par.to_s) #unless eval(par.to_s).nil?
            end
            return @master.call(File.join(PATH, "cancella_automobile"), _params)
        end

        def cambia_stato_automobile(stato: , id_automobile_esterno: , id_automobile_interno: , note: nil)
            _keys = method(__method__).parameters.collect(&:last)
            # _optional_keys = method(__method__).parameters.to_h[:key].collect
            _params = {}
            _keys.each do |par|
              _params[par] = eval(par.to_s) #unless eval(par.to_s).nil?
            end
            return @master.call(File.join(PATH, "cambia_stato_automobile"), _params)
        end

        def lista_marche(nome_marca: nil)
          _keys = method(__method__).parameters.collect(&:last)
          # _optional_keys = method(__method__).parameters.to_h[:key].collect
          _params = {}
          _keys.each do |par|
            _params[par] = eval(par.to_s) unless eval(par.to_s).nil?
          end
          return @master.call(File.join(PATH, "lista_marche"), _params)
        end

        def lista_modelli(nome_marca: nil, nome_modello: nil, id_marca: nil)
          _keys = method(__method__).parameters.collect(&:last)
          # _optional_keys = method(__method__).parameters.to_h[:key].collect
          _params = {}
          _keys.each do |par|
            _params[par] = eval(par.to_s) unless eval(par.to_s).nil?
          end
          return @master.call(File.join(PATH, "lista_modelli"), _params)
        end

        def lista_automobili(id_automobile_esterno: nil, id_automobile_interno: nil, targa: nil)
          _keys = method(__method__).parameters.collect(&:last)
          # _optional_keys = method(__method__).parameters.to_h[:key].collect
          _params = {}
          _keys.each do |par|
            _params[par] = eval(par.to_s) unless eval(par.to_s).nil?
          end
          return @master.call(File.join(PATH, "lista_automobili"), _params)
        end
    end

end
