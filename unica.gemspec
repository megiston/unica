Gem::Specification.new do |s|
    s.name = 'unica'
    s.version = '0.0.1'
    s.summary = 'A Ruby API library for the Unica platform.'
    s.description = 'A Ruby API library for the Unica platform.'
    s.authors = ['azaupa at Megiston s.r.l.']
    s.email = 'azaupa@megiston.com'
    s.files = ['lib/unica.rb', 'lib/unica/api.rb', 'lib/unica/errors.rb']
    s.homepage = 'https://bitbucket.org/megiston/unica-ruby/'
    s.add_dependency 'json', '>= 1.7.7'
    s.add_dependency 'excon', '>= 0.73.0'
end
