require 'rubygems'
require 'excon'
require 'json'

require 'unica/errors'
require 'unica/api'

module Unica
    class API

        attr_accessor :session, :host, :username, :password, :debug, :path

        def initialize(username=nil, password=nil, host=nil, debug=false)
            @host = host || ENV['UNICA_HOST'] || 'https://www.consorziounica.com/'
            @path = ENV['UNICA_BASE_PATH'] || "/ws/web_service"
            @username = username || ENV['UNICA_USERNAME']
            @password = password || ENV['UNICA_PASSWORD']

            raise Error, 'You must provide Host Username and Password' unless @username
            raise Error, 'You must provide Host Username and Password' unless @password
            raise Error, 'You must provide Host Username and Password' unless @host
            @session = Excon.new @host
            @debug = debug
        end

        def call(url, params={})
            params[:username] = @username
            params[:password] = @password
            params = JSON.generate(params)
            r = @session.post(:path => File.join(@path, url), :headers => {'Content-Type' => 'application/json'}, :body => params)

            cast_error(r.body) if r.status != 200
            return JSON.parse(r.body)
        end

        def get(url, params={})
          params[:username] = @username
          params[:password] = @password
          params = JSON.generate(params)
          r = @session.get(:path => File.join(@path, url), :headers => {'Content-Type' => 'application/json'}, :body => params)
          cast_error(r.body) if r.status != 200
          return JSON.parse(r.body)

        end


        def cast_error(body)

            error_map = {
                'ValidationError' => ValidationError,
            }

            begin
                error_info = JSON.parse(body)
                if error_info['status'] != 'error' or not error_info['name']
                    raise Error, "We received an unexpected error: #{body}"
                end
                if error_map[error_info['name']]
                    raise error_map[error_info['name']], error_info['error']
                else
                    raise Error, error_info['error']
                end
            rescue JSON::ParserError
                raise Error, "We received an unexpected error: #{body}"
            end
        end

        def automobili
          Unica::Automobili.new(self)
        end

    end
end
