Overview
=============================================
A Ruby API client for [v1 of the Unica API](https://consorziounica.orangepix.it)


###Usage
This can be installed via our gem using:
```
$ gem install unica-ruby
```
Then you'll need to `require 'unica-ruby'` in your code.

Alternatively, if you're using a recent version of Bundler, you can add the following to your Gemfile:
```
gem 'unica-ruby', require: 'unica-ruby'
```
Then `bundle install` and you're all set.


---

Make the following constants, in your environment file or where ever you are mentioning the constants.


    UNICA_USER = "xxxx"
    UNICA_PASSWORD = "xxxxx"

UseCases
--------

Following line will create the object of unica api, we will access the unica-ruby method using this object.

    unica = Unica::API.new(UNICA_USER, UNICA_PASSWORD)
    unica.cambia_stato_automobile(stato: "stato", id_automobile_esterno: 2, id_automobile_interno: 3, note: "opzionale")

Every method implemented in the api is paired with public method of Unica::API object.

response

{
    "stato": true,
    "messaggio": "La trattativa é passata in stato di \"successo\" quindi l'automobile é già stata processata come una vendita",
    "dati": []
}

OR

{
    "stato": false,
    "messaggio": "La trattativa risulta essere in stato di \"successo\" quindi l'automobile é già stata processata come una vendita",
    "dati": []
}
