module Unica
    class Error < StandardError
    end
    class ValidationError < Error
    end
    class ServerError < Error
    end
end
